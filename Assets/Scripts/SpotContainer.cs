using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotContainer
{
    List<Spot> free_spots = new List<Spot>();

    List<Spot> busy_spots = new List<Spot>();

    public void AddSpot(Spot spot)
    {
        free_spots.Add(spot);
    }

    public Spot GetFree()
    {
        if (free_spots.Count > 0)
        {
            int spot = Random.Range(0, free_spots.Count);
            busy_spots.Add(free_spots[spot]);
            free_spots.RemoveAt(spot);
            return busy_spots[busy_spots.Count - 1];
        }

        return null;
    }

    public void SetFree(GameObject enemy)
    {
        Spot spot = busy_spots.Find(x => x.enemy == enemy);
        busy_spots.Remove(spot);
        spot.ClearSpot();
        free_spots.Add(spot);
    }

    public void Clear()
    {
        foreach (Spot spot in busy_spots)
        {
            spot.ClearSpot();
            free_spots.Add(spot);
        }

        busy_spots.Clear();
    }
}
