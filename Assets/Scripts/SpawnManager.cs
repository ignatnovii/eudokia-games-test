using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class SpawnManager : MonoBehaviour
{
    public Spot boss_spot;
    public List<Spot> spot_object;
    public SpotContainer spots;
    public float waiting_time_min = 5f;
    public float waiting_time_max = 10f;

    public bool spawnable;
    public bool boss;

    private void Awake()
    {
        spots = new SpotContainer();
        spawnable = false;
        boss = false;

        foreach (Spot unit in spot_object)
        {
            spots.AddSpot(unit);
        }
    }

    private void Update()
    {
        if (spawnable)
            StartCoroutine(SpawnEnemy());
    }

    IEnumerator SpawnEnemy()
    {
        spawnable = false;

        Spot spot = spots.GetFree();
        if (spot != null)
        {
            if (boss)
            {
                boss_spot.SetSpot(GameManager.instance.big_mob[Random.Range(0, GameManager.instance.big_mob.Count)], GameManager.instance.current_big_gold);
                boss = false;
            }
            else
            {
                spot.SetSpot(GameManager.instance.lil_mob[Random.Range(0, GameManager.instance.lil_mob.Count)], GameManager.instance.current_lil_gold);
            }
        }
        else
        {
            spawnable = false;
            GameManager.instance.menu.GameOver();
        }

        yield return new WaitForSeconds(Random.Range(waiting_time_min, waiting_time_max) * GameManager.instance.current_time_multiplier);
        spawnable = true;
    }
}
