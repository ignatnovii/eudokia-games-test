using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public AnimationClip enter;
    public AnimationClip exit;
    public AnimationClip go_up;
    public AnimationClip go_down;
    public AnimationClip records_up;
    public AnimationClip records_down;

    public Text records;

    public bool gameover;
    bool showed;
    bool records_showed;
    Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        ShowMenu();
        showed = true;
        gameover = false;
        records_showed = false;
    }

    public void ShowMenu()
    {
        // ���� �� � ����
        if (!showed)
        {
            if (!gameover)
            {
                Time.timeScale = 0;
                anim.Play(enter.name);
            }
            else
            {
                anim.Play(go_down.name);
            }
            showed = true;
        }
        else
        {   // ���� � ����
            if (records_showed)
            {
                records_showed = false;
                anim.Play(records_down.name);
                return;
            }

            if (!gameover)
            {
                showed = false;
                Time.timeScale = 1;
                anim.Play(exit.name);
            }
        }
    }

    public void NewGame()
    {
        gameover = false;
        showed = false;
        Time.timeScale = 1;

        anim.Play(exit.name);
        GameManager.instance.spawn_manager.spawnable = true;
        GameManager.instance.spawn_manager.spots.Clear();

        GameManager.instance.SaveState();
        GameManager.instance.Restart();
    }

    public void Records()
    {
        records_showed = true;
        anim.Play(records_up.name);

        if (!PlayerPrefs.HasKey("SaveState"))
        {
            return;
        }

        string[] data = PlayerPrefs.GetString("SaveState").Split('|');

        records.text = "RECORDS:";

        foreach (string s in data)
        {
            records.text += s + Environment.NewLine;
        }
    }

    public void Credits()
    {
        // �������� ��� ������
    }

    public void ExitGame()
    {
        GameManager.instance.SaveState();
        GameManager.instance.saved = true;

        Application.Quit();
    }

    public void GameOver()
    {
        gameover = true;
        Time.timeScale = 0f;
        anim.Play(go_up.name);
    }
}
