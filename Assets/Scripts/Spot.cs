using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ����� ��� �����, �� ������� ��������� ������

public class Spot : MonoBehaviour
{
    public Transform place { get; private set; }
    public GameObject enemy { get; private set; }
    public bool is_available { get; private set; }

    private void Awake()
    {
        place = gameObject.transform;
        is_available = true;
    }

    public void SetSpot(GameObject enemy, int coins)
    {
        if (is_available)
        {
            this.enemy = Instantiate(enemy, place);
            enemy.GetComponent<Enemy>().coins = coins;
            is_available = false;
        }
    }

    public void ClearSpot()
    {
        if (!is_available)
        {
            Destroy(enemy);
            enemy = null;
            is_available = true;
        }
    }
}
