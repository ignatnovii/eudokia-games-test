using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Clickable
{
    public int hp = 3;
    public int coins = 1;
    public int points = 10;

    public AnimationClip anim;

    protected override void Awake()
    {
        base.Awake();

        GetComponent<Animator>().Play(anim.name);
    }

    protected override void OnMouseDown()
    {
        base.OnMouseDown();

        hp -= GameManager.instance.damage;

        if (hp <= 0)
            Death();
    }

    void Death()
    {
        if (GameManager.instance.spawn_manager.boss_spot.enemy == gameObject)
            GameManager.instance.spawn_manager.boss_spot.ClearSpot();
        else
            GameManager.instance.spawn_manager.spots.SetFree(gameObject);
        GameManager.instance.AddCoins(coins, points);
        GameManager.instance.AddKill();
        GameManager.instance.saved = false;
        Destroy(gameObject);
    }
}
