using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public List<GameObject> big_mob;
    public List<GameObject> lil_mob;
    public SpawnManager spawn_manager;
    public Menu menu;

    public int damage = 1;
    public int gold = 0;
    public int points = 0;

    public int start_lil_gold = 1;
    public int start_big_gold = 3;
    public int current_lil_gold;
    public int current_big_gold;

    public float start_time_multiplier = .9f;
    public float current_time_multiplier = 1f;

    public int killed;
    int last_killed;

    public bool saved = true;

    void Awake()
    {
        if (GameManager.instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);

        gold = 0;
        points = 0;
        current_lil_gold = start_lil_gold;
        current_big_gold = start_big_gold;

        killed = 0;
        saved = true;
    }

    public void AddCoins(int coins, int points)
    {
        gold += coins;
        this.points += points;
    }

    public void AddKill()   // ����������� ���������/������� �� ������ 10 ������������ ��������
    {
        killed++;
        if (killed / 10 > last_killed)
        {
            last_killed = killed / 10;
            current_big_gold += start_big_gold;
            current_lil_gold += start_lil_gold;

            current_time_multiplier *= start_time_multiplier;

            spawn_manager.boss = true;
        }
    }

    public void SaveState()
    {
        if (saved)
            return;
        string s = PlayerPrefs.GetString("SaveState") + "|" + points.ToString();

        PlayerPrefs.SetString("SaveState", s);
    }

    public void Restart()
    {
        damage = 1;
        gold = 0;
        points = 0;
        start_lil_gold = 1;
        start_big_gold = 3;
        current_lil_gold = start_lil_gold;
        current_big_gold = start_big_gold;
        killed = 0;
        last_killed = 0;
        saved = true;
    }
}
