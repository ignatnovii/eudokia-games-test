using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clickable : MonoBehaviour
{
    public float scale_up = 1.2f;
    public float color_speed = 10f;
    public float tilt_angle = 5f;
    public Color click_color = Color.red;

    SpriteRenderer sprite;
    Color target_color;
    Color current_color = Color.white;

    protected virtual void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
        target_color = sprite.color;
        current_color = sprite.color;
    }

    protected virtual void OnMouseDown()
    {
        transform.localScale = Vector3.one * scale_up;
        transform.localRotation = Quaternion.Euler(0, 0, tilt_angle * (1 - 2 * Random.Range(0, 2)));
        target_color = click_color;
    }

    protected virtual void OnMouseUp()
    {
        transform.localScale = Vector3.one;
        transform.localRotation = Quaternion.Euler(0, 0, 0);
        target_color = Color.white;
    }

    protected virtual void Update()
    {
        current_color = sprite.color;
        sprite.color = new Color(target_color.r, Mathf.Lerp(current_color.g, target_color.g, Time.deltaTime * color_speed), Mathf.Lerp(current_color.b, target_color.b, Time.deltaTime * color_speed));
    }
}
